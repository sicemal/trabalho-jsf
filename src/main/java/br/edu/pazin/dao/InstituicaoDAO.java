/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.pazin.dao;

import br.edu.pazin.modelo.Instituicao;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
/**
 *
 * @author limeira
 */
public class InstituicaoDAO extends GenericDAO<Instituicao> {
    public Instituicao getPorId(Integer id) {
        try {
            return (Instituicao) getSessao().get(Instituicao.class, id);
        } catch (Exception e) {
            System.out.println("TipoUsuarioDAO.getPorId()");
            return null;
        }
    }

    public List<Instituicao> getTodosTipoUsuarios() {
        try {
            Criteria c = getSessao().createCriteria(Instituicao.class);
            c.addOrder(Order.asc("papel"));
            return c.list();
        } catch (Exception ex) {
            System.out.println("TipoUsuarioDAO.getTodosTipoUsuarios()");
            return null;
        }
    }

    public String salvar(Instituicao tp){
        String msg = "";
        try {
            if (getPorId(tp.getId()) == null) {
                this.inserir(tp);
                msg = "Instituição inserido com sucesso!";
            } else {
                this.alterar(tp);
                msg = "Instituição atualizado com sucesso!";
            }
            return msg;
        } catch (Exception ex) {
            return "Problemas ao salvar instituição: " + ex.getMessage();
        }
    }  
    
}
