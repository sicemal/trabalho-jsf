/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.pazin.controle;

import br.edu.pazin.dao.InstituicaoDAO;
import br.edu.pazin.modelo.Instituicao;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author limeira
 */
@ManagedBean(name="instituicaoBean")
@ViewScoped
public class InstituicaoBean {
    private FacesContext fc;
    private FacesMessage fm;
    private String msg;
    private int idTab;
    private Instituicao instituicao;

    private void prepararTela() {
        instituicao = new Instituicao();
        instituicao.setNome("Insira o nome...");
        instituicao.setEndereco("Insira o endereco...");
        fc = FacesContext.getCurrentInstance();
    }    
    public Instituicao getInstituicao() {
        return instituicao;
    }
    
    public void onTabChange(TabChangeEvent event) {
        if (event.getTab().getId().equalsIgnoreCase("lista")) {
            idTab = 0;
        } else if (event.getTab().getId().equalsIgnoreCase("cadastro")) {
            idTab = 1;
        }
    }    
    
    public InstituicaoBean()
    {
        prepararTela();
    }

    public int getIdTab() {
        return idTab;
    }

    public void setIdTab(int idTab) {
        this.idTab = idTab;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void limparTela() {
        prepararTela();
    }

    public void alterar(Instituicao instituicao) {
        this.instituicao = instituicao;
        InstituicaoDAO dao = new InstituicaoDAO();
//        listaPerfilUsuario
//                = dao.listarTiposPerfisUsuario(user.getId());
        this.idTab = 1;
    }

    public void excluir(Instituicao instituicao) {
        InstituicaoDAO dao = new InstituicaoDAO();
        dao.excluir(instituicao);
//        listaTodosUsuarios = null;
        fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Exclusão",
                "Instituicao excluída com sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, fm);
    }

    public void salvar(Instituicao instituicao) {
        InstituicaoDAO dao = new InstituicaoDAO();
        dao.salvar(instituicao);
//        listaTodosUsuarios = null;
        fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Exclusão",
                "Instituicao excluída com sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, fm);
    }
   
}
    

